#!/bin/bash

check_input() {
  echo "enter name:"
	exec 3<>/dev/tty
	read -u 3 SIG_NAME

	if [[ -z "$SIG_NAME" ]]; then
		echo "missing name"
		exit 1
	fi

  echo "enter job title:"
	exec 3<>/dev/tty
	read -u 3 SIG_JOB_TITLE

	if [[ -z "$SIG_JOB_TITLE" ]]; then
		echo "missing job title"
		exit 1
	fi

  echo "done."
}

download_signature() {
	echo "downloading signature..."

	SIGNATURE_URL="https://bitbucket.org/supernovadesign/newangle-email-signature/raw/1c1e47963822af1c83342504ac7f2fab88c185e9/signature.html"
	SIGNATURE_HTML=`curl -s "$SIGNATURE_URL"`

	if [[ -z "$SIGNATURE_HTML" ]]; then
		echo "empty signature url response"
		exit 1
	fi

	echo "done."
}

setup_signature() {
	echo "setting up signature..."

  UPDATED_SIGNATURE_HTML=${SIGNATURE_HTML/__NAME__/$SIG_NAME}
  UPDATED_SIGNATURE_HTML=${UPDATED_SIGNATURE_HTML/__TITLE__/$SIG_JOB_TITLE}

	SIGNATURE_HTML_ESCAPED=`echo "$UPDATED_SIGNATURE_HTML" | sed -e 's/"/\\\"/g'`

	osascript <<END
tell application id "com.microsoft.Outlook"
	make new signature with properties {name:"signature", content:"$SIGNATURE_HTML_ESCAPED"}
end tell
END

	if [ -f "$HOME/Library/Preferences/com.microsoft.Outlook.plist" ]; then
		defaults write "$HOME/Library/Preferences/com.microsoft.Outlook.plist" "AutomaticallyDownloadExternalContent" -int 2
		defaults write "$HOME/Library/Preferences/com.microsoft.Outlook.plist" "Send Pictures With Document" -int 0
	fi

	if [ -f "$HOME/Library/Containers/com.microsoft.Outlook/Data/Library/Preferences/com.microsoft.Outlook.plist" ]; then
		defaults write "$HOME/Library/Containers/com.microsoft.Outlook/Data/Library/Preferences/com.microsoft.Outlook.plist" "AutomaticallyDownloadExternalContent" -int 2
		defaults write "$HOME/Library/Containers/com.microsoft.Outlook/Data/Library/Preferences/com.microsoft.Outlook.plist" "Send Pictures With Document" -int 0
	fi

	echo "done."
}

check_input
download_signature
setup_signature
