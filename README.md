**Make changes to the signature template**

1. Edit the `signature.html` file
2. Push the changes back to this repository
3. Update the `SIGNATURE_URL` path in `apple-mail.sh` and `outlook-mac.sh` to reference the new path to `signature.html`
4. Push the changes to `apple-mail.sh` and `outlook-mac.sh` back to this repository


**Generate an email signature for Apple Mail**

1. Open Apple Mail application
2. Go to Mail > Preferences > Signatures
3. Click the + button to create a blank signature
4. Quit Apple Mail
5. Launch Terminal.app (you can find this in Applications > Utilities)
6. Enter the command below and press return
`curl https://bitbucket.org/supernovadesign/newangle-email-signature/raw/f5524232d437fa05259dc3a8a238d8101bae0290/apple-mail.sh | bash`
7. When prompted enter a name (e.g. Adam Smith), and job title (e.g. CEO)
8. Once the script is finished, re-open Apple Mail and find your new signature in Preferences > Signatures.

**Generate an email signature for Outlook (Mac)**

1. Launch Terminal.app (you can find this in Applications > Utilities)
2. Enter the command below and press return
`curl https://bitbucket.org/supernovadesign/newangle-email-signature/raw/f5524232d437fa05259dc3a8a238d8101bae0290/outlook-mac.sh | bash`
3. When prompted enter a name (e.g. Adam Smith), and job title (e.g. CEO)
4. Once the script is finished, open Outlook and find your new signature in Preferences > Signatures.


**Instructions for other email clients**

1. Download `signature.html` and open the file in a web browser
2. Copy the HTML to the clipboard
3. Launch your email client
4. Create a new email signature
5. Paste the HTML into the new signature and edit the `__NAME__` and `__TITLE__` placeholders.